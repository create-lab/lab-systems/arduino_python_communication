from comms_wrapper import *

def main():
    '''
    KEYBOARD LISTENER

    If you want to use key commands in your process, create the Key() instance.
    '''
    key = Key()

    '''
    DEFINE ARDUINOS

    Define the Arduino() instance as shown below. 
    descriptiveDeviceName: Name of the device you choose. e.g.: "Measurment arduino uno"
    portName: Port the arduino is connected. Check in your teminal/device manager
              e.g.: "/dev/tty/ACM0" on linux,  "COM1" on windows, "/dev/tty.usbserial1 on MacOS"
    baudrate: The baudrate defined in your Arduino .ino file (Serial.begin(BAUDRATE);)

    You can define as many Arduinos as you have connected. e.g.: see arduino2 below arduino1

    Defining arduinos does not estabilish a connection. 
    '''
    arduino1 = Arduino(descriptiveDeviceName="myArduino", portName="/dev/ttyACM0", baudrate=115200)

    # As an example, we have arduino2 defined. All following functions can use this too.
    #arduino2 = Arduino(descriptiveDeviceName="myArduino2", portName="/dev/ttyACM2", baudrate=1000000)

    '''
    CONNECTING WITH THE ARDUINO

    Call the connect_and_handshake() method to estabilish a reliable connection with the arduino. 
    You must perform this for every arduino.
    '''
    arduino1.connect_and_handshake()

    
    # Your main loop/process
    while 1: 
        
        '''
        RECEIVING MESSAGES
        
        This is how you receive messages from the arduino side
        Calling receive_message() will read the serial bus.
        This will be stored in the class variable receivedMessages
        '''
        arduino1.receive_message()

        '''
        The boolean newMsgReceived is useful to know if new messages have come in
        rather than printing the old message many times
        '''
        if arduino1.newMsgReceived:
            print(arduino1.receivedMessages)


        '''
        SENDING MESSAGES

        You can send a message (or messages) by simply calling send_message()

        If you want to send a message, simply write your message as an argument e.g.: send_message(20)
        If you want to send multiple messages, do this through a list e.g.: send_message([10,30,"hello"])
        '''

        '''
        KEY COMMANDS

        If you created an instance of Key(), you can use your keyboard easily.
        Read your key commands by calling keyPress. 
        This will return the key press while the key is pressed. When released, it will return None.
        If you instead want the stored most recent key press, use keyPressedLatching
        '''
        if key.keyPress == "f": 
            arduino1.send_message("apples")

        if key.keyPress == "g":
            arduino1.send_message("bananas")

        '''
        DEBUGGING
        
        Comment out the below code to debug the system
        This will show you relevant info to see how your system is behaving
        
        You can obtain all the information by specifying the verbose mode => arduino1.debug(verbose = True))
        '''
        arduino1.debug()

        '''
        This delay is not critical. It is only in place to 
            a) Demonstrate any process blocking the execution does not affect the communication
            b) Make the printout easy to read
        '''
        time.sleep(0.2)


if __name__ == '__main__':
    main()