# Changelog
All notable changes to this project will be documented in this file

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

<!-- 
## [Unreleased]

### Added

### Changed

### Removed -->


## [Unreleased]

### Added

### Changed

### Removed


## [Release v1.0.0](https://gitlab.epfl.ch/create-lab/lab-systems/arduino_python_communication/-/releases/release_v1.1.0) - 17-4-2022

### Added
- Status variable `newMsgRecieved` to indicate when a messege with new data is received
- Documentation on the functions and variables
- Option to send multiple messages from python to arduino side
- Testing data on different OS with different Arduinos

### Changed
- Threading to be killed when the main python file is terminated
- Bugfix on communication to be slow on a windows machine
- Moved several public funtions to private

### Removed
- Removed requirement to specify the message name on the python side
- Removed disconnect function as not needed

## [Release v1.0.0](https://gitlab.epfl.ch/create-lab/lab-systems/arduino_python_communication/-/releases/release_v1.0.0) - 22-1-2022

### Added
- Debugging funcionality to understand the communictaion setup. Notably to easily see what the Arduino side has received from the Python side
- Obtain latching and non-latching key press outputs

### Changed
- Spelling mistakes fixed for the word "received"
- Addressed and patched the issue of the Arduino not sending messages in the first few seconds after connection/handshake is estabilished
- Fixed the issue of messages not being received properly on the Python side
- Simplified the connection process on the Arduino side
- Simplified the communication process on the Arduino side
- A range of variable and function names to be more descriptive
- Organised private and public methods in the Arduino class

### Removed
- Unnecessary functions and variables in the Arduino class
- Functions that were neither in the Arduino nor the Key class
