#include "pyCommsLib.h"

/*
Define the message names you want send from the Arduino side 
to the Python side. 
*/
String msgName[] = {"msgA", "msgB", "msgC"};


/*
Define the C arry which will carry the data content. 
The number (here it's 2) must match the number of messages you have defined.
*/
String dataCarrier[3];


void setup() {
  // Start the serial communication.
  Serial.begin(115200);

  // Connect with the Python side
  init_python_communication();
}

void loop() {
  
  // This is how you access the latest received message from the Python side
  // You can obtain the FULL MESSAGE by this
  String received_message = latest_received_msg();

  // If you are sending multiple messages from the python side, 
  // you can specify the index value as an argument in latest_received_msg(i);
  // e.g.: String received_message = latest_received_msg(1);

  /*
  Populate the dataCarrier array with your data
  Here we have some temporary data/messages.
  You must ALWAYS send strings and no other variable type. 
  You can convert anything to a string by calling String(); 
  */
  dataCarrier[0] = String(millis());
  dataCarrier[1] = String(3);
  dataCarrier[2] = "Another message";

  /*
   * This command "loads" the message. It doesn't send the message yet.
   */
  load_msg_to_python(msgName, dataCarrier, size_of_array(msgName));


  /*
   * The sync() function must be declared at every loop of your arduino code 
   * sync() will 
   *    a) Receive messages from the Python side
   *    b) Send loaded messages to the Python side
   */
  sync();

  /*
   * This delay is not critial. 
   * It is here to show that having a process that blocks the loop can be used.
   * e.g.: a sensor with a very slow sampling speed. 
   */
  delay(100);
}
